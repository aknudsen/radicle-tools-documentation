# Radicle Tools Documentation

Documentation of my collection of Radicle tools.

## Requirements
The documentation is written in [Asciidoctor](https://asciidoctor.org) syntax. On OS X
we recommend installing it with Homebrew: `brew install asciidoctor`.

## Building
To build the documentation in HTML format, just invoke asciidoctor on the main document:
```
$ asciidoctor index.adoc
```
